import { apiStatus } from '../../../lib/util';
import { Router } from 'express';

const Magento2Client = require('magento2-rest-client').Magento2Client;

module.exports = ({ config, db }) => {
  let mcApi = Router();

  mcApi.post('/contact', (req, res) => {
    let contactData = req.body
    if (!contactData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('contact', (restClient) => {
      var module = {};
      module.contact = function () {
        return restClient.post('/contact', contactData)
      }
      return module;
    })

    client.contact.contact().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/socialSignup', (req, res) => {
    let socialData = req.body
    if (!socialData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);
    client.addMethods('socialSignup', (restClient) => {
      var module = {};
      module.socialSignup = function () {
        return restClient.put('/aureatelabs/socialSignup', socialData)
      }
      return module;
    })

    client.socialSignup.socialSignup().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/getrewards', (req, res) => {
    let customerId = req.body.u_id
    if (!customerId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('rewards', (restClient) => {
      var module = {};
      module.rewards = function () {
        return restClient.get('/getrewardshistory?customer_id=' + customerId)
      }
      return module;
    })

    client.rewards.rewards().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/getproductrewards', (req, res) => {
    let productId = req.body.product_id
    if (!productId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('rewards', (restClient) => {
      var module = {};
      module.rewards = function () {
        return restClient.get('/aureatelabs-productrewardpoint/getpoints?product_id=' + productId)
      }
      return module;
    })

    client.rewards.rewards().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/getfreegiftproducts', (req, res) => {
    let quoteId = req.body.quote_id
    if (!quoteId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('giftproducts', (restClient) => {
      var module = {};
      module.giftproducts = function () {
        return restClient.get('/aureatelabs-freegift/getdata?quote_id=' + quoteId)
      }
      return module;
    })

    client.giftproducts.giftproducts().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/addfreegift', (req, res) => {
    let quote = req.body.quote
    let rule = req.body.rule
    let gift = req.body.gift

    if (!quote && !rule && !gift) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('addgiftproducts', (restClient) => {
      var module = {};
      module.addgiftproducts = function () {
        return restClient.post('/mpfreegifts/gift/add/quote/' + quote + '/rule/' + rule + '/gift/' + gift)
      }
      return module;
    })

    client.addgiftproducts.addgiftproducts().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/applyRewardPoint', (req, res) => {
    let rewardPoint = req.body.rewardPoint
    let cartId = req.body.cartId

    if (!rewardPoint && !cartId) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }
    const client = Magento2Client(config.magento2.api);
    client.addMethods('addRewardPoint', (restClient) => {
      var module = {};
      module.addRewardPoint = function () {
        let postUrl = '/mirasvitrewards/' + cartId + '/apply/' + rewardPoint
        return restClient.post(postUrl)
      }
      return module;
    })

    client.addRewardPoint.addRewardPoint().then((result) => {
      apiStatus(res, result, 200); // just dump it to the browser, result = JSON object
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/addCardData', (req, res) => {
    let tokenData = req.body.postData

    if (!tokenData) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);

    client.addMethods('storecardinfo', (restClient) => {
      var module = {};
      module.storecardinfo = function () {
        let postUrl = '/aureatelabs-customerapi/customers'
        return restClient.post(postUrl, tokenData)
      }
      return module;
    })

    client.storecardinfo.storecardinfo().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/fetchCardData', (req, res) => {
    let email = req.body.email

    if (!email) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);

    client.addMethods('fetchcardinfo', (restClient) => {
      var module = {};
      module.fetchcardinfo = function () {
        let postUrl = '/aureatelabs-customerapi/customers/search?searchCriteria[filterGroups][0][filters][0][field]=email&searchCriteria[filterGroups][0][filters][0][value]=' + email
        return restClient.get(postUrl)
      }
      return module;
    })

    client.fetchcardinfo.fetchcardinfo().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/fetchWishlistData', (req, res) => {
    let customer_id = req.body.customer_id

    if (!customer_id) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);

    client.addMethods('fetchwishlist', (restClient) => {
      var module = {};

      module.fetchwishlist = function () {
        let postUrl = '/wishlist/' + customer_id
        return restClient.get(postUrl)
      }
      return module;
    })

    client.fetchwishlist.fetchwishlist().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/addWishlistProduct', (req, res) => {
    let sku = req.body.sku
    let customer_id = req.body.customer_id

    if (!customer_id && !sku) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);

    client.addMethods('addwishlist', (restClient) => {
      var module = {};

      module.addwishlist = function () {
        let postUrl = '/wishlist/' + sku + '/' + customer_id
        return restClient.put(postUrl)
      }
      return module;
    })

    client.addwishlist.addwishlist().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/removeWishlistProduct', (req, res) => {
    let item_id = req.body.item_id
    let customer_id = req.body.customer_id

    if (!customer_id && !item_id) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);

    client.addMethods('removewishlist', (restClient) => {
      var module = {};

      module.removewishlist = function () {
        let postUrl = '/wishlist/' + item_id + '/' + customer_id
        return restClient.delete(postUrl)
      }
      return module;
    })

    client.removewishlist.removewishlist().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  mcApi.post('/clearWishlist', (req, res) => {
    let customer_id = req.body.customer_id

    if (!customer_id) {
      apiStatus(res, 'Internal Server Error!', 500)
      return
    }

    const client = Magento2Client(config.magento2.api);

    client.addMethods('clearwishlist', (restClient) => {
      var module = {};

      module.clearwishlist = function () {
        let postUrl = '/wishlist/clear/' + customer_id
        return restClient.delete(postUrl)
      }
      return module;
    })

    client.clearwishlist.clearwishlist().then((result) => {
      apiStatus(res, result, 200);
    }).catch(err => {
      apiStatus(res, err, 500);
    })
  })

  return mcApi
}